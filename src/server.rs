//! Server interaction functions

use std::{collections::HashMap, sync::mpsc::Sender};
use protobuf::Message;
use std::sync::mpsc::{channel, Receiver};
use crate::rics::RICS_Response;
use super::rics;

unsafe impl Send for RICSClient {}
unsafe impl Sync for RICSClient {}
pub struct RICSClient {
    socket: zmq::Socket,
    node_names: HashMap<i32, String>,
    node: i32,
}

pub trait NodeName {
    fn get_name(&self, svr: &mut RICSClient) -> Option<i32> {
        svr.list_nodes();
        self.get_name_cached(svr)
    }

    fn get_name_cached(&self, svr: &RICSClient) -> Option<i32>;
}

impl NodeName for String {
    fn get_name_cached(&self, svr: &RICSClient) -> Option<i32> {
        svr.node_from_name_cached(self.as_str()).get(0).cloned()
    }
}

impl NodeName for i32 {
    fn get_name(&self, _svr: &mut RICSClient) -> Option<i32> {
        Some(*self)
    }

    fn get_name_cached(&self, _svr: &RICSClient) -> Option<i32> {
        Some(*self)
    }
}

pub enum ConnectTo {
    Default,
    ZmqAddr(String),
}

impl RICSClient {

    /// Returns the default socket connected to the default connection
    pub fn default_socket(ctx: &zmq::Context) -> zmq::Socket {
        let sock = ctx.socket(zmq::DEALER).unwrap();
        sock.connect("ipc://rics.socket").unwrap();
        sock
    }

    pub fn try_default_socket(ctx: &zmq::Context) -> zmq::Result<zmq::Socket> {
        let sock = ctx.socket(zmq::DEALER)?;
        sock.connect("ipc://rics.socket")?;
        Ok(sock)
    }

    /// New server connection using default settings
    pub fn new(ctx: &zmq::Context) -> zmq::Result<RICSClient> {
       let socket = RICSClient::try_default_socket(ctx)?;

       Ok(RICSClient {
           socket,
           node_names: HashMap::new(),
           node: 0,
       })
    }

    pub fn get_socket(&self) -> &zmq::Socket {&self.socket}

    /// CPS server creation
    pub fn with_server<T, F>(ct: ConnectTo, f: F) -> T
    where F: FnOnce(RICSClient) -> T {
        let ctx = zmq::Context::new();
        // Build server
        let server = match ct {
            ConnectTo::Default => RICSClient::new(&ctx).expect("Failed to connect to server"),
            ConnectTo::ZmqAddr(path) => {
                let socket = ctx.socket(zmq::DEALER).expect("Failed to create client socket");
                socket.connect(&path).expect("Filed to connect to server");
                RICSClient {
                    socket,
                    node_names: HashMap::new(),
                    node: 0
                }
            }
        };
        f(server)
    }

    /// Send a formatted message to the zmq socket.
    pub fn send_msg<T>(&self, msg: T) where T: Message {
        // self.socket.send(Vec::<u8>::new(), zmq::SNDMORE).expect("Send message failed");
        let mut v: Vec<u8> = Vec::new();
        msg.write_to_vec(&mut v).expect("Error formatting message");
        // self.socket.send(v, 0).expect("Send message failed");
        self.socket.send_multipart([Vec::new(), v].iter(), 0).expect("Send message failed");
    }

    /// Receive a formatted message from the zmq socket
    pub fn recv_msg_flags<T>(&self, flags: i32) -> zmq::Result<T> where T: Message {
        self.socket.recv_bytes(flags)?;
        // TODO Should be on
        self.socket.get_rcvmore()?;
        let vec = self.socket.recv_bytes(0)?;
        T::parse_from_bytes(&vec).map_err(|_err| zmq::Error::EINVAL)
    }

    pub fn recv_msg<T>(&self) -> zmq::Result<T> where T: Message {
        self.recv_msg_flags(0)
    }



    /// Create a connection to the socket using the default rics daemon socket location.
    pub fn connect(&self, as_node: bool) {
        debug!("Connecting..");
        let mut req = rics::RICS_Request::new();
        let mut msg = rics::RICS_Connection::new();
        msg.set_connect_as_node(as_node);
        req.set_connection(msg);
        self.send_msg(req);
        trace!("Connection message sent");
    }

    /// Get the current connection id. Also sets the internal number variable.
    pub fn who_am_i(&mut self) ->i32 {
        debug!("Sending WHO_AM_I query");
        let mut msg = rics::RICS_Request::new();
        msg.set_query(rics::RICS_Request_RICS_Query::WHO_AM_I);
        self.send_msg(msg);

        trace!("Waiting for server response");
        self.node = self.recv_msg::<RICS_Response>().expect("Error reading who_am_i response").get_node();
        trace!("Done processing response");
        self.node
    }

    /// Sets the CAN drop rate for the server
    /// A value of 0 means all messages are forwarded,
    /// while a value of 1 means that no messages are.
    pub fn set_can_drop_chance(&self, v: f32) {
        let mut msg = rics::RICS_Request::new();
        msg.set_can_drop_chance(v);
        self.send_msg(msg);
    }

    /// Sets the CAN broadcast flag for the server
    /// A CAN broadcast active means that when a data packet
    /// has CAN type, it will be send to every node regardless
    /// of routing.
    ///
    /// The server does not send a confirmation.
    pub fn set_can_broadcast(&self, v: bool) {
        
        debug!("Changing can broadcast flag to {}", v);

        let mut msg = rics::RICS_Request::new();
        msg.set_query(if v { rics::RICS_Request_RICS_Query::SET_FLAG_CAN_BROADCAST } else { rics::RICS_Request_RICS_Query::CLEAR_FLAG_CAN_BROADCAST });
        self.send_msg(msg);
    }


    /// Return the currently loaded nodes and their alias
    pub fn list_nodes(&mut self) -> &HashMap<i32, String> {
        debug!("Sending LIST_SINK query");
        let mut msg = rics::RICS_Request::new();
        msg.set_query(rics::RICS_Request_RICS_Query::LIST_SINK);
        self.send_msg(msg);
        trace!("Waiting for server response");
        let resp = self.recv_msg::<RICS_Response>().expect("Error Listing nodes");
        self.node_names = resp.get_idlist().get_ids().iter().map(|id| (id.get_id(),id.get_name().into())).collect();
        trace!("Done processing response, got {:?}", self.node_names);
        &self.node_names
    }

    /// Stops the parent server
    pub fn stop_server(&self) {
        debug!("Stopping server");
        let mut msg = rics::RICS_Request::new();
        msg.set_query(rics::RICS_Request_RICS_Query::DAEMON_QUIT);
        self.send_msg(msg);
    }

    pub fn node_from_string_cached(&self, str: impl Into<String>) -> Option<i32> {
        let str = str.into();
        str.parse::<i32>().ok().and_then(|n| n.get_name_cached(self)).or_else(|| str.get_name_cached(self))
    }

       
    pub fn node_from_string(&mut self, str: impl Into<String>) -> Option<i32> {
        let str = str.into();
        str.parse::<i32>().ok().and_then(|n| n.get_name(self)).or_else(|| str.get_name(self))
    }

    /// Get the node id from a name
    pub fn node_from_name_cached(&self, name: impl Into<String>) -> Vec<i32> {
        let mut v = vec![];
        let name = name.into();
        for (i, n) in &self.node_names {
            if n == &name {
                v.push(*i);
            }
        }
        v
    }

    /// Get the node id from a name
    pub fn node_from_name(&mut self, name: impl Into<String>) -> Vec<i32> {
        self.list_nodes();
        self.node_from_name_cached(name)
    }

    /// High-level delete route between two nodes
    pub fn del_route(&mut self, from: impl NodeName, to: impl NodeName) -> bool{
        if let Some(f) = from.get_name(self) {
            if let Some(t) = to.get_name_cached(self) {
                let mut route = rics::RICS_Route::new();
                route.set_from(f);
                route.set_to(t);

                let mut req = rics::RICS_Request::new();
                req.set_del_route(route);

                self.send_request(req);
                true
            } else {
                false
            }
        } else {
            false
        }
    }

    /// High-level add route between two nodes
    pub fn add_route(&mut self, from: impl NodeName, to: impl NodeName) -> bool{
        if let Some(f) = from.get_name(self) {
            if let Some(t) = to.get_name_cached(self) {
                let mut route = rics::RICS_Route::new();
                route.set_from(f);
                route.set_to(t);

                let mut req = rics::RICS_Request::new();
                req.set_add_route(route);

                self.send_request(req);
                true
            } else {
                false
            }
        } else {
            false
        }
    }


    /// Send a request to the server
    pub fn send_request(&self, msg: rics::RICS_Request) {
        debug!("Request sent as: {:?}", &msg);
        self.send_msg(msg);
    }

    /// Send a generic RICS data packet to a specific node
    pub fn send_packet_to(&self, data: rics::RICS_Data, target: i32) {
        let mut msg = rics::RICS_Request::new();
        let mut data = data;
        debug!("Data sent: {:?}; To: {}", &data, target);
        data.set_target(target);
        msg.set_data(data);
        self.send_request(msg)
    }

    /// Send a generic RICS data packet to the default route
    pub fn send_packet(&self, data: rics::RICS_Data) {
        let mut msg = rics::RICS_Request::new();
        let mut data = data;
        data.clear_target();
        debug!("Data sent: {:?}", &data);
        msg.set_data(data);
        self.send_request(msg)
    }

    /// Blocks and wait for the next server message
    pub fn get_response(&mut self) -> Option<rics::RICS_Response> {
        debug!("Getting packet...");
        match self.recv_msg::<rics::RICS_Response>() {
            Ok(resp) => Some(resp),
            Err(err) => { warn!("GET_PACKET bad response: {}", err); None},
        }
    }

    /// Blocks and wait for the next packet
    pub fn get_packet(&mut self) -> Option<rics::RICS_Data> {
        debug!("Getting packet...");

        let resp = self.recv_msg::<rics::RICS_Response>().ok()?;
        if resp.has_data() {
            let data = resp.get_data();
            Some(data.clone())
        } else {
            None
        }
    }

}


pub fn can_packet(id: i32, dat: Vec<u8>) -> rics::RICS_Data {
    let mut data = rics::RICS_Data::new();
    data.set_id(id);
    data.set_data(dat);
    data.set_field_type(rics::RICS_Data_RICS_DataType::CAN);
    data
}

pub fn stream_packet(dat: Vec<u8>) -> rics::RICS_Data {
    let mut data = rics::RICS_Data::new();
    data.set_data(dat);
    data.set_field_type(rics::RICS_Data_RICS_DataType::STREAM);
    data
}


pub fn data_to_string(data: &rics::RICS_Data) -> String {
    format!("<{} -> {} ({:08x}) [{}]>", data.get_source(),
            data.get_target(),
            data.get_id(),
            data.get_data().iter().map(|x| format!("{:02x}", x))
            .collect::<Vec<String>>().join(", "))
}

pub fn data_to_loggable_string(data: &rics::RICS_Data) -> String{
    let time = std::time::SystemTime::now();
    let datetime: chrono::DateTime<chrono::offset::Local> = time.into();
    format!("{},{:x},{},{}", datetime.format("%Y-%m-%d %T%.3f"), data.get_id(), data.get_data().len(), data.get_data().iter()
             .map(|x|format!("{:x}",x)).collect::<Vec<String>>().join(","))
}

pub fn response_to_string(resp: &rics::RICS_Response) -> String {
    if resp.has_node() {
        format!("<WHO_AM_I: {}>", resp.get_node())
    } else if resp.has_data() {
        data_to_string(resp.get_data())
    } else {
        format!("<???>")
    }
}
