//! Server hosting features

use std::process;
use bimap::BiMap;
use protobuf::Message;
use std::collections::HashMap;
use super::rics;
use rand;
use rand::Rng;


/// Contains server permanent state
pub struct ServerState {
    /// Flag for if the CAN broadcasting is enabled
    can_broadcast: bool,
    /// Determines the likelyhood of a CAN message being dropped
    /// in CAN mode, if a message is dropped, no one receives the
    /// message
    can_drop_chance: f32,
    /// Internal flag for node id allocation
    node_allocator: i32,
    /// Holds the self described names of the nodes
    node_names: HashMap<i32, String>,
    /// ZMQ Addresses of the nodes
    node_addresses: BiMap<i32, u32>,
    /// Current loading routes
    node_routing: HashMap<i32, Vec<i32>>,
}

impl ServerState {

    pub fn new() -> ServerState {
        ServerState {
            can_broadcast: true,
            can_drop_chance: 0.00,
            node_allocator: 0,
            node_names: HashMap::new(),
            node_addresses: BiMap::new(),
            node_routing: HashMap::new(),
        }
    }

    fn get_can_broadcast(&self) -> bool {self.can_broadcast}

    fn set_can_broadcast(&mut self, broadcast: bool) {
        self.can_broadcast = broadcast;
    }

    fn get_node_names(&self) -> &HashMap<i32, String> {
        &self.node_names
    }


    fn delete_node(&mut self, node: i32) {
        if node + 1 == self.node_allocator {
            self.node_allocator -= 1;
        }

        self.node_addresses.remove_by_left(&node);
        self.node_names.remove(&node);
        self.node_routing.remove(&node);
    }

    fn new_node(&mut self, addr: u32) -> i32 {
        let n = self.node_allocator;
        self.node_allocator += 1;
        self.node_names.insert(n, n.to_string());
        self.node_addresses.insert(n, addr);
        n
    }

    fn set_node_name(&mut self, node: i32, name: impl Into<String>) {
        let str = name.into();
        trace!("Setting node {} to name {}", node, str.clone());
        self.node_names.insert(node, str);
    }

    fn rename_node(&mut self, node: i32, name: impl Into<String>) -> bool {
        if self.node_names.contains_key(&node) {
            self.set_node_name(node, name);
            true
        } else {
            false
        }
    }

    fn add_route(&mut self, node: i32, target: i32) {
        match self.node_routing.get_mut(&node) {
            Some(vec) => if !vec.contains(&target) { vec.push(target); } ,
            None => (),
        }
    }

    fn del_route(&mut self, node: i32, target: i32) {
        match self.node_routing.get_mut(&node) {
            Some(vec) => vec.retain(|&x| x != target),
            None => (),
        }
    }

    fn set_can_drop_chance(&mut self, v: f32) {
        if v >= 0.0 && v <= 1.0 {
            self.can_drop_chance = v;
            info!("Changing CAN drop rate to {}", v);
        } else {
            warn!("Invalid CAN drop value: {}", v);
        }
    }
}

/// Send a formatted response to the given address
fn respond_to<T>(socket: &zmq::Socket, addr: u32, msg: &T) where T: protobuf::Message {
    socket.send([0u8, (addr & 0xff) as u8, ((addr >> 8) & 0xff) as u8, ((addr >> 16) & 0xff) as u8, ((addr >> 24) & 0xff) as u8].as_slice(), zmq::SNDMORE)
          .expect("Error sending frame");
    socket.send([].as_slice(), zmq::SNDMORE).expect("Error sending frame");
    let mut v: Vec<u8> = Vec::new();
    msg.write_to_vec(&mut v).expect("Error formatting vector");
    socket.send(v, 0).expect("Error sending data");
}

/// Arbitrary client connection manager
pub fn run_server(mut state: ServerState, socket: zmq::Socket) -> !{
    let mut rng = rand::thread_rng();
    // Message managing loop
    loop {
        let vecs = socket.recv_multipart(0).expect("Can't receive from server");
        if vecs.len() != 3 { continue; }
        // Read the message. Router adds the address part.
        let address_vec = &vecs[0];
        let address: u32 = (address_vec[1] as u32) + ((address_vec[2] as u32) << 8)
            + ((address_vec[3] as u32) << 16) + ((address_vec[4] as u32) << 24);
        let data = &vecs[2];

        match rics::RICS_Request::parse_from_bytes(data) {
            Ok(req) => {
                let node = state.node_addresses.get_by_right(&address).map(|x| *x);

                if req.has_connection() {
                    if req.get_connection().get_connect_as_node() {
                        let nd = state.new_node(address);
                        state.set_node_name(nd, format!("{}",nd)); // Default name
                        debug!("Creating node id {}", nd);
                    } else {
                        debug!("Opening info connection");
                    }
                } else if req.has_set_name() {
                    node.map(|x| state.rename_node(x, req.get_set_name()));
                } else if req.has_query() {
                    match req.get_query() {
                        rics::RICS_Request_RICS_Query::NULL => (),
                        rics::RICS_Request_RICS_Query::LIST_SINK => {
                            let mut response = rics::RICS_Response::new();
                            let mut idlist = rics::RICS_Response_RICS_IdList::new();
                            let ids: Vec<_> = state.get_node_names().iter().map(|(k,v)| {
                                let mut id = rics::RICS_Response_RICS_Id::new();
                                id.set_id(*k);
                                id.set_name(v.clone());
                                trace!("Reply node pair: {} - {}", *k, v.clone());
                                id
                            }).collect();
                            idlist.set_ids(protobuf::RepeatedField::from_vec(ids));
                            response.set_idlist(idlist);
                            respond_to(&socket, address, &response)
                        },
                        rics::RICS_Request_RICS_Query::WHO_AM_I => {
                            debug!("Answer WHO_AM_I request with {:?}", node);
                            let mut msg = rics::RICS_Response::new();
                            node.map(|n| msg.set_node(n));
                            respond_to(&socket, address, &msg);
                        },
                        rics::RICS_Request_RICS_Query::SET_FLAG_CAN_BROADCAST => state.set_can_broadcast(true),
                        rics::RICS_Request_RICS_Query::CLEAR_FLAG_CAN_BROADCAST => state.set_can_broadcast(false),
                        // TODO Add delete/unregister node
                        rics::RICS_Request_RICS_Query::DAEMON_QUIT => process::exit(2),
                    }
                    ()
                } else if req.has_data() {
                    // Packet message, must forward
                    let mut msg = rics::RICS_Response::new();
                    let mut data = req.get_data().clone();
                    if let Some(n) = node { data.set_source(n); }
                    msg.set_data(data.clone());

                    // Broadcast Dropping
                    if state.can_drop_chance != 0.0 && data.get_field_type() == rics::RICS_Data_RICS_DataType::CAN {
                        if rng.gen::<f32>() < state.can_drop_chance {
                            info!("Server is dropping packet {:?}", data);
                            continue;
                        }
                    }

                    // Forwarding
                    if state.get_can_broadcast() && data.get_field_type() == rics::RICS_Data_RICS_DataType::CAN {
                        // CAN broadcast forwarding
                        for (n,a) in state.node_addresses.iter() {
                            if Some(*n) != node {
                                respond_to(&socket, *a, &msg);
                            }
                        }
                    } else {
                        // Routing forwarding
                        for target in if data.has_target() {
                            vec![data.get_target()]
                        } else {
                            node.and_then(|n| state.node_routing.get(&n).cloned()).unwrap_or(vec![])
                        } {
                            if let Some(&addr) = state.node_addresses.get_by_left(&target) {
                                info!("Forwarding to {}", target);
                                respond_to(&socket, addr, &msg);
                            }
                        }
                    }
                } else if req.has_add_route() {
                    let i = req.get_add_route().get_from();
                    let j = req.get_add_route().get_to();
                    state.add_route(i, j);
                } else if req.has_del_route() {
                    let i = req.get_del_route().get_from();
                    let j = req.get_del_route().get_to();
                    state.del_route(i, j);
                } else if req.has_can_drop_chance() {
                    state.set_can_drop_chance( req.get_can_drop_chance() );
                } else {
                    warn!("Invalid message {:?}", req);
                }
            },
            Err(err) => { warn!("Invalid message query {}", err); },
        }
    }
}
