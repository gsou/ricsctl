
# Table of Contents

1.  [ricsctl](#org45bdbd6)
    1.  [Installation](#org3d1fcec)
        1.  [Windows](#org7e099cd)
        2.  [Linux](#org91bcddc)
    2.  [Usage](#org6692e9d)
        1.  [Server configuration](#orgced52ff)
        2.  [Server usage](#orgfee989a)
        3.  [Lua API](#org907383a)
        4.  [Dynlib API](#org29f14f5)
        5.  [Rust API](#org936ceca)



<a id="org45bdbd6"></a>

# ricsctl

ricsctl, or Remote Inter-Connected Streams is a server, client and library that allows the virtualisation of bus and stream connections. Typical usage include adding virtual or simulated modules to a bus such as a CAN bus, or allow easy control and monitoring of busses.

Its primary purpose is transmitting CAN bus data over WiFi.

<a id="org3d1fcec"></a>

## Installation


<a id="org7e099cd"></a>

### Windows (might be outdated, try branch v0.2)

1. Install https://www.msys2.org/
2. Open MSYS2 UCRT64
3. Install dev tools:

       pacman -S --needed --noconfirm $MINGW_PACKAGE_PREFIX-gcc $MINGW_PACKAGE_PREFIX-rust $MINGW_PACKAGE_PREFIX-pkgconf make git

4. Install dependencies:

       pacman -S --needed --noconfirm $MINGW_PACKAGE_PREFIX-libusb
       pacman -U --noconfirm https://gist.github.com/PhilCS/7c8215682587a130662ac88050c98349/raw/mingw-w64-ucrt-x86_64-lua-5.4.4-1-any.pkg.tar.zst

5. Install ricsctl:

       git clone https://codeberg.org/gsou/ricsctl.git
       RUSTFLAGS=-Awarnings cargo install --path ricsctl --features pluginlua --root $MSYSTEM_PREFIX

6. (optional) Install MoonFLTK:

       pacman -S --needed --noconfirm $MINGW_PACKAGE_PREFIX-fltk
       git clone https://github.com/stetre/moonfltk.git
       make -C moonfltk && mkdir -p $MSYSTEM_PREFIX/lib/lua/5.4/ && mv -f moonfltk/src/moonfltk.dll $_

ricsctl can then be executed from the MSYS2 UCRT64 terminal.

If usage via regular Windows command line is needed, add `C:\msys64\ucrt64\bin` to your `PATH` environment variable:

    powershell -Command "[Environment]::SetEnvironmentVariable('PATH', (Get-Item 'HKCU:\Environment').GetValue('PATH', '', 'DoNotExpandEnvironmentNames') + ';C:\msys64\ucrt64\bin', 'User')"


<a id="org91bcddc"></a>

### Linux

Requirements:

-   protoc
-   lua 5.4

Installation:

    cargo install --path .


<a id="org6692e9d"></a>

## Usage

RICS uses a server client approach in order to connect multiple processes to each other. Each client can send different types of messages to each other in a configurable routing pattern. Each client is also able to configure the server.

RICS can use any of the ømq connection interfaces. The server can be started to listen on multiple connections from the command line. The default socket is `ipc://rics.socket`.

To configure the allowed connection points, start the server with the `-c` command line argument, specifying a valid ømq endpoint.

Examples:

    ricsctl start # Starts the server listening only on the default connection point
    ricsctl -c tcp://*:1000 start # Starts the server listening only on the tcp socket port 1000
    ricsctl -c ipc://path.socket start # Starts the server listening only on the ømq interprocess socket path.socket.
    ricsctl -c tcp://*:1000 -c tcp://lo:80000 -c ipc://uds.socket start # Start the server listening on the three provided locations

Once the server is started, multiple packet types can be sent. The following table shows all the supported packet types. Multiple options can be used to configure the server response to different protocol types

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Name</th>
<th scope="col" class="org-left">Description</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">RAW</td>
<td class="org-left">Raw packet type, used internally</td>
</tr>


<tr>
<td class="org-left">STREAM</td>
<td class="org-left">Part of a stream-like communication protocol</td>
</tr>


<tr>
<td class="org-left">CAN</td>
<td class="org-left">CAN bus emulation</td>
</tr>


<tr>
<td class="org-left">DATAGRAM</td>
<td class="org-left">Datagram based communication protocol</td>
</tr>


<tr>
<td class="org-left">SYNC</td>
<td class="org-left">Syncing packet for timing critical protocols</td>
</tr>
</tbody>
</table>

The environment variable `RUST_LOG` can be used to configure the verbosity of the output. `RUST_LOG=trace` will show all debug information.


<a id="orgced52ff"></a>

### Server configuration

All the following commands can use the `-c` option to select the server to configure.

    ricsctl list

Lists the connected nodes.

    ricsctl stop

Stops the connected server.

    ricsctl route SOURCE -t TARGET1 TARGET2

Connect the node named `SOURCE` to the targets `TARGET1`, `TARGET2`. Add the flag `-d` to disconnect instead. All messages sent by `SOURCE` will be received by `TARGET1` and `TARGET2`, but not the other way around.

    ricsctl can broadcast true/false

Sets the CAN broadcast flag. If the CAN broadcast is set to true, all messages of type CAN will be sent to every other node.


<a id="orgfee989a"></a>

### Server usage

The command line tool also provide some common client operations.

    ricsctl log

Display all received messages.

    ricsctl can connect CANIFACE

Connects the server to the socketcan interface CANIFACE. (Linux only)

    ricsctl can serial PORT [BAUD]

Connect to a serial to CANbus converter. I use this internally, please open a issue if you are interested in using this.

    ricsctl can log

Display all received can messages.

    ricsctl can send --id 12 --data '{0,1,2,3}'

Send a CAN message. The data and id parameters must be valid Lua.

    ricsctl plugin --lua file.lua
    ricsctl plugin --dynlib dynlib.dll/dynlib.so

Provides a easy way to run a user application on the server. The Lua and Dynlib api are described below.


<a id="org907383a"></a>

### Lua API

The lua file must contain the following lua functions:

    function rics_start(svr, node) -- Called on server connection (node is your node number)
      return true
    end
    
    function rics_update(svr) -- Called frequently
      return true
    end
    
    function rics_init() -- Called as soon as the Lua engine is loaded
      return true
    end
    
    function rics_can_callback(svr, id, data) -- Called when a can message is received
      return true
    end


<a id="org29f14f5"></a>

### TODO Dynlib API

Please open an issue if you are interested in using this.


<a id="org936ceca"></a>

### Rust API

A rust library can be used to build user applications. The rust documentation can be built with `cargo doc`.

A simple example of using the RICS library:

    extern crate rics;
    use rics::server;
    
    fn main() {
        server::RICSClient::with_server(server::ConnectTo::Default, move|mut svr| {
          svr.connect(true); // Connect as node
          let node = svr.who_am_i(); // Get node number
          println!("My node name is: {}", node);
          svr.send_packet(server::can_packet(12, vec![node])); // Send the node id on packet
        });
    }

